resource "aws_ebs_volume" "volume" {
  size              = var.volume_size
  availability_zone = var.availability_zone

  tags = {
    Name = "${var.workspace_id}-volume"
  }
}
