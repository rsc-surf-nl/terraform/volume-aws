output "id" {
  value = aws_ebs_volume.volume.id
}

output "volume_id" {
  value = aws_ebs_volume.volume.id
}
