variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "availability_zone" {}
variable "aws_region" {}

variable "volume_size" {
  type    = number
  default = 0
}

variable "assume_role_arn" {
  default = ""
}
variable "assume_role_session_name" {
  default = ""
}
variable "aws_session_token" {
  type        = string
  description = "Optional; the AWS session token"
  default     = ""
}

# SRC automatic variables are added explicitly to be self-contained
variable "application_type" {
  type        = string
  description = "The application type (eg. 'Compute', 'Storage' etc.)"
  default     = "Storage"
}
variable "cloud_type" {
  type        = string
  description = "Cloud type (eg. 'AWS', 'Openstack', 'Azure' etc.)"
  default     = "AWS"
}
variable "co_id" {
  type        = string
  description = "The ID of the associated CO"
  default     = "unset"
}
variable "resource_type" {
  type        = string
  description = "The resource type (eg. 'VM', 'Storage-Volume' etc.)"
  default     = "Storage-Volume"
}
variable "subscription" {
  type        = string
  description = "The (SRC) subscription under which this resources is created"
  default     = "unset"
}
variable "subscription_group" {
  type        = string
  description = "The subscription group to which the `subscription` belongs"
  default     = "unset"
}
variable "wallet_id" {
  type        = string
  description = "The ID of the associated wallet"
  default     = "unset"
}
variable "workspace_fqdn" {
  type        = string
  description = "The FQDN assigned to this workspace"
  default     = "example.surf-hosted.nl"
}
variable "workspace_id" {
  type        = string
  description = "The ID of this workspace"
  default     = "unset"
}
